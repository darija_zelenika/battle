<?php

class Army {
    private $title;
    private $num_solders;
    private $weapons;
    private $defence_level;
    private $attacking_weapon;

    function __construct( $title, $num_solders, $type ) {
        $this->title = $title;
        $this->num_solders = $num_solders;
        $this->weapons = array();

        if ($type == 'agressive') {
            array_push($this->weapons, new Weapon('Machine gun', 1, 5));
            array_push($this->weapons, new Weapon('Sword', 1, 3));
            array_push($this->weapons, new Weapon('Granade', 3, 9));
            array_push($this->weapons, new Weapon('Airplane', 5, 15));
            $this->defence_level = 2;
        }  else if ($type == 'strategic') {
            array_push($this->weapons, new Weapon('Pistol', 1, 4));
            array_push($this->weapons, new Weapon('Rifal', 2, 5));
            array_push($this->weapons, new Weapon('Mines', 2, 7));
            array_push($this->weapons, new Weapon('Rocket', 4, 11));
            $this->defence_level = 4;
        } else if ($type == 'defensive') {
            array_push($this->weapons, new Weapon('Revolver', 1, 2));
            array_push($this->weapons, new Weapon('Shootgun', 2, 6));
            array_push($this->weapons, new Weapon('Sniper', 2, 8));
            array_push($this->weapons, new Weapon('Carbines', 3, 10));
            $this->defence_level = 6;
        }
  }

    function get_title() {
      return $this->title;
    }

    function is_defending() {
        if (rand(1, 10) < $this->defence_level){
            return true;
        }
        return false;
    }

    function wounded($damage){
    	if($damage > $this->num_solders) {
    		$this->num_solders = 0;
    	}else {
    		$this->num_solders -= $damage;
    	}   
    }

    function get_num_solders(){
        return $this->num_solders;
    }

    function select_weapon() {
        $this->attacking_weapon = $this->weapons[rand(0, count($this->weapons)-1)];

        return $this->attacking_weapon->get_title();
    }

    function attack($army_in_defence){
     
        return $this->attacking_weapon->fire($army_in_defence);
    }
}

?>