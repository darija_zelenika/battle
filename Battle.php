<?php

class Battle {
    private $on_move;
    private $in_defence;
    private $battle_log = array();
   
    function choose_first_attacker($player_1, $player_2){
        if(rand(1,10)>5) {
            $this->on_move = $player_1;
            $this->in_defence = $player_2;
        } else {
            $this->in_defence = $player_1;
            $this->on_move = $player_2;
        }
    }

    function switch_atacker() {
        $temp = $this->on_move;
        $this->on_move = $this->in_defence;
        $this->in_defence = $temp;
    }

    function start($player_1, $player_2) {
        $this->choose_first_attacker($player_1, $player_2);
      
       while ($player_1->get_num_solders() > 0 && $player_2->get_num_solders() > 0) {
            $this->attack();
        }
       
        // return battle log at the end of battle and print its output 
        return $this->battle_log;
    }

    function attack() {
    
        $weapon_title = $this->on_move->select_weapon();
      
        // if total damage is 0 then army defended itself from attack
       
        $total_damage = $this->on_move->attack($this->in_defence);
        
        $this->battle_log[] = array('attacker' => $this->on_move->get_title(),
    								'defending' => $this->in_defence->get_title(),
    								'weapon' => $weapon_title,
    								'killed_soldier' => $total_damage,
    								'number_soldier_attacker' => $this->on_move->get_num_solders(),
    								'number_soldier_defending' => $this->in_defence->get_num_solders());
 
        $this->switch_atacker();   
    }
}
?>