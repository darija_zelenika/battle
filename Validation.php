<?php

class Validation {
	private $army1;
	private $army2;
	private $typeArmy1;
	private $typeArmy2;

	function check_validation( $get ) {
		$error = array();

		if(!isset($get['army1'])) {
	    array_push($error, "Army 1 is missing. Set Get parameter army1!");
		}

		if(!isset($get['army2'])) {
	    array_push($error, "Army 2 is missing. Set Get parameter army2!");
		}

		if(!isset($get['army1_type'])) {
	    array_push($error, "Army1 type is missing. Set Get parameter army1_type agressive, strategic or defensive!");
		}
		if(!isset($get['army2_type'])) {
	    array_push($error, "Army2 type is missing. Set Get parameter army2_type agressive, strategic or defensive!");
		}

		return $error;
	}

	function is_value_valid ($get) {
		$armyType = array('agressive', 'strategic', 'defensive');
		$error = array();
		
		if(!is_numeric($get['army1'])) {
			array_push($error, "Army1 should be numeric !");
		}

		if(!is_numeric($get['army2'])) {
			array_push($error, "Army2 should be numeric !");
		}

		if(!in_array($get['army1_type'],$armyType)) {
			array_push($error, "Army1 type should have one of the following values : agressive, strategic or defensive !");
		}

		if(!in_array($get['army2_type'],$armyType)) {
			array_push($error, "Army2 type should have one of the following values : agressive, strategic or defensive !");
		}

		return $error;
	}
}
?>