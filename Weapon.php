<?php

class Weapon {
    private $title;
    private $min_damage;
    private $max_damage;

    function __construct( $title, $min_damage, $max_damage ) {
    $this->title = $title;
    $this->min_damage = $min_damage;
        $this->max_damage = $max_damage;
    }

    function fire($army) {
      
        if(!$army->is_defending()) {
            $damage = rand($this->min_damage, $this->max_damage);
           
            $army->wounded($damage);
            return $damage;
        }

        return 0;
    }

    function get_title() {
      return $this->title;
    }
}
?>