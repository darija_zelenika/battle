<html>
<?php 

require_once ('Battle.php');
require_once ('Army.php');
require_once ('Weapon.php');
require_once ('Validation.php');

?>
<body>

<?php

$validation = new Validation;

$error = $validation->check_validation($_GET);

if (count($error) > 0) {
  foreach($error as $item) {
    echo "<p>" . $item . "</p>";
  }

  exit();
} 

$error = $validation->is_value_valid($_GET);

if (count($error) > 0) {
  foreach($error as $item) {
    echo "<p>" . $item . "</p>";
  }

} else {
  $titleArmy1 = 'army1';
  $titleArmy2 = 'army2';

  echo "<p>Army 1 " . $_GET[$titleArmy1] . " vojnika</p>";
  echo "<p>Army 2 " . $_GET[$titleArmy2] . " vojnika</p>";
  echo "<p> Tijek borbe: </p>";
  
  $army1 = new Army($titleArmy1, $_GET[$titleArmy1],$_GET['army1_type']);
  
  $army2 = new Army($titleArmy2, $_GET[$titleArmy2],$_GET['army2_type']);  

  $battle = new Battle;
  $battle_log = $battle->start($army1,$army2);

  foreach ($battle_log as $key => $value) {
    
   echo '<p>' .($key+1). '. Vojska '. $value['attacker']. ' je napala sa ' .$value['number_soldier_attacker']. ' vojnika vojsku ' .$value['defending']. ' sa oružjem ' .$value['weapon']. ' te ubila ' .$value['killed_soldier'] .' vojnika. Vojsci '.$value['defending'].' je ostalo ' .$value['number_soldier_defending']. ' vojnika. </p><br>';
}

if($army1->get_num_solders() > $army2->get_num_solders()) {
    echo  "Pobijedila je vojska Army1 kojoj je ostalo ".$army1->get_num_solders(). " vojnika.";
  }else {
    echo "Pobijedila je vojska Army2 kojoj je ostalo ".$army2->get_num_solders(). " vojnika.";
  }
}

?>
</body>
</html>